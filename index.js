import express from 'express';
import mongoose from 'mongoose';
import graphlHTTP from 'express-graphql';
import schema from './schema';
import cors from 'cors';

const app = express();
const port = 4000;
const dbName = "test"; // new
const uri = `mongodb+srv://elly:aoaka0301@cluster0-hgktn.mongodb.net/test?retryWrites=true&w=majority`
mongoose.Promise = global.Promise;
mongoose.connect(uri, { useNewUrlParser: true });

app.use(cors());

app.use(`/graphql`, graphlHTTP({
    schema: schema,
    graphiql: true
})); // new

app.listen(port, () => {
    console.log(`서버 실행!! 포트는? ${port}`);
});
