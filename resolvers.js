import Patient from './models/patient';
export const resolvers = {
    Query: {
        async getPatient(root, { _id }){
          console.log("get a patient " + _id);
          return await Patient.findById(_id);
        },
        async allPatient() {
            console.log("get all patient");
            return await Patient.find();
        }
    },

    Mutation: {
      async createPatient(root, { input }) {
          return await Patient.create(input);
      },
      async updatePatient(root, { _id, input }) {
        return await Patient.findOneAndUpdate(
            { _id }, 
            input, 
            { new: true }
        );
      },
      async deletePatient(root, { _id }) {
        return await Patient.findOneAndDelete({ _id });
      }
  }
}
