import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const PatientSchema = new Schema({
  chartno: {
    type: String,
    required: true,
    unique : true
  },
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  photo_url: {
    type: String
  },
  birthdate: {
    type: String,
    required: true
  },
  gender: {
    type: String
  }
});

export default mongoose.model('patient', PatientSchema);