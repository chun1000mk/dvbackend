import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ImageSchema = new Schema({
  chartno: {
    type: String,
    required: true
  },
  created_date: {
    type: String,
    required: true
  },
  thumbnail_url: {
    type: String,
    required: true
  },
  modality: {
    type: String,
    required: true
  },
});

export default mongoose.model('image', ImageSchema);