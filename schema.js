import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from './resolvers'

const typeDefs = `
    type Patient {
        _id: ID!
        chartno: String!
        firstname: String!
        lastname: String!
        photo_url: String
        birthdate: String!
        gender: String
    }
    type Query {
        getPatient(_id: ID!): Patient
        allPatient: [Patient]
    }

    input PatientInput {
      chartno: String!
      firstname: String!
      lastname: String!
      photo_url: String
      birthdate: String!
      gender: String
  }

    type Mutation {
      createPatient(input: PatientInput): Patient
      updatePatient(_id: ID!, input: PatientInput): Patient
      deletePatient(_id: ID!): Patient
    }
`;

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

export default schema;